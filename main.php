<?php

require "vendor/autoload.php";
include('library/config/config.inc.php');
include('library/include/Stejk/autoload.php');

// read the configuration file
$dataDir = getenv('KBC_DATADIR') . DIRECTORY_SEPARATOR;
$configFile = $dataDir . 'config.json';
$config = json_decode(file_get_contents($configFile), true);

$multiplier = $config['parameters']['multiplier'];

// create input file and write header
$outFile = new \Keboola\Csv\CsvFile(
    $dataDir . 'out' . DIRECTORY_SEPARATOR . 'tables' . DIRECTORY_SEPARATOR . 'RHIEM.csv'
);
$outFile->writeRow(['name', 'quantity', 'date']);


// Get warehouse stat
$deSklad = new \Stejk\PowerBI\RhiemSklad();
$dePrediction = new \Stejk\PowerBI\Prediction($deSklad);
$dePrediction->getSkladZasoby();
$dePostFields = $dePrediction->create();

$outFile->writeRow([
    "DRINK",
    $dePostFields->skladDrinkActual,
    $dePostFields->date
]);

$outFile->writeRow([
    "POWDER",
    $dePostFields->skladPowderActual,
    $dePostFields->date
]);


try {

} catch (InvalidArgumentException $e) {
    echo $e->getMessage();
    exit(1);
} catch (\Throwable $e) {
    echo $e->getMessage();
    exit(2);
}
