<?php

namespace Stejk\PowerBI;

class Prediction
{
  public $url;
  public $sklad;
  public $history;

  public $stavSklad = array();
  public $avarage = array();
  public $prediction = array();
  public $numOfDays = array();

  public function __construct($url = "", $countries = [], $mode = "NOT", \Stejk\PowerBI\SkladInterface $sklad)
  {
    $this->url = $url;
    $this->sklad = $sklad;

    $this->history = new \Stejk\PowerBI\History($countries, $mode);
  }

  public function getSkladZasoby()
  {
    $this->stavSklad = array(
      "bag" => $this->sklad->getStav("bag"),
      "drink" => $this->sklad->getStav("drink")
    );
  }

  public function getAvarages()
  {
    $this->avarage[14] = $this->history->getAvarage(14);
    $this->avarage[30] = $this->history->getAvarage(30);
    $this->avarage[90] = $this->history->getAvarage(90);
    $this->avarage[180] = $this->history->getAvarage(180);
  }

  public function predict()
  {
    $this->prediction = array(
      "14" => array(
        "bag" => $this->stavSklad["bag"]/$this->avarage[14]["bag"],
        "drink" => $this->stavSklad["drink"]/$this->avarage[14]["drink"]
      ),
      "30" => array(
        "bag" => $this->stavSklad["bag"]/$this->avarage[30]["bag"],
        "drink" => $this->stavSklad["drink"]/$this->avarage[30]["drink"]
      ),
      "90" => array(
        "bag" => $this->stavSklad["bag"]/$this->avarage[90]["bag"],
        "drink" => $this->stavSklad["drink"]/$this->avarage[90]["drink"]
      ),
      "180" => array(
        "bag" => $this->stavSklad["bag"]/$this->avarage[180]["bag"],
        "drink" => $this->stavSklad["drink"]/$this->avarage[180]["drink"]
      )
    );
  }

  public function create()
  {
    $postFieldsObj = new \stdClass();
    $postFieldsObj->date = gmdate('Y-m-d\TH:i:s\Z');
    $postFieldsObj->skladDrinkActual = round($this->stavSklad["drink"]);
    $postFieldsObj->skladPowderActual = round($this->stavSklad["bag"]);

    $postFieldsObj->predictionDrink14 = round($this->prediction["14"]["drink"]);
    $postFieldsObj->predictionPowder14 = round($this->prediction["14"]["bag"]);
    $postFieldsObj->predictionDrink30 = round($this->prediction["30"]["drink"]);
    $postFieldsObj->predictionPowder30 = round($this->prediction["30"]["bag"]);
    $postFieldsObj->predictionDrink90 = round($this->prediction["90"]["drink"]);
    $postFieldsObj->predictionPowder90 = round($this->prediction["90"]["bag"]);
    $postFieldsObj->predictionDrink180 = round($this->prediction["180"]["drink"]);
    $postFieldsObj->predictionPowder180 = round($this->prediction["180"]["bag"]);

    $this->numOfDays["30"] = $this->history->getStatsForDays(30);
    $this->numOfDays["60"] = $this->history->getStatsForDays(60);

    $postFieldsObj->statDrink30 = round($this->numOfDays["30"]["drink"]);
    $postFieldsObj->statPowder30 = round($this->numOfDays["30"]["bag"]);
    $postFieldsObj->statDrink60 = round($this->numOfDays["60"]["drink"]);
    $postFieldsObj->statPowder60 = round($this->numOfDays["60"]["bag"]);


    $postFieldsObj->avarageDrink = round($this->avarage[30]["drink"]);
    $postFieldsObj->avaragePowder = round($this->avarage[30]["bag"]);

    $postFields = array($postFieldsObj);

    return $postFields;
  }

  public function send($postFields='')
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($postFields),
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/json",
        "postman-token: f0da61da-da8b-cdc7-ad4e-a7d9b5cb9d96"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo(json_encode($postFields));

      echo json_encode($response);
    }
  }
}
