<?php

namespace Stejk\PowerBI;

interface SkladInterface
{
  public function getStav($type);
}
