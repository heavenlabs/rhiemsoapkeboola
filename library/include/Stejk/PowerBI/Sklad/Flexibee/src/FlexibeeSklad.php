<?php

namespace Stejk\PowerBI;

class FlexibeeSklad implements \Stejk\PowerBI\SkladInterface
{
  public function getStav($type)
  {
    $flexiBeeREST = new \FlexibeeRestClass(HOST, FIRMA, USERNAME, PASSWORD, SHOPID);

    $sklad = $flexiBeeREST->getStavSkladuKDatu();
    $stavSklad = array();

    foreach ($sklad as $polozka) {
        if($polozka->cenik == "code:SACEK"){
          $stavSklad["bag"] = $polozka->stavMJ;
        } elseif($polozka->cenik == "code:MANA DRINK 3 SKU"){
          $stavSklad["drink"] = $polozka->stavMJ * 12;
        }
    }

    return $stavSklad[$type];
  }
}
