<?php
/*
* 2013 Flexibee Restapi class
*
* NOTICE OF LICENSE
*
* Redistribution and use in source and binary forms, with or without
* modification, are not permitted
*
*  @author Lukáš Krchňák <info@uniqueweb.cz>
*  @copyright  2013 FlexibeeRestapi
*  @version  Release: $Revision: 10 $
*  @license  All rights reservered
*/

require_once dirname(__FILE__).'/include/Httpful/Bootstrap.php';
require_once dirname(__FILE__).'/include/Log.php';

//use \Httpful\Request;

/**
 * FlexibeeRestClass.
 *
 * @author Lukáš Krchňák
 * @copyright 2013
 *
 * @version $Id$
 */
class FlexibeeRestClass
{
    private $host = null;

    private $firma = null;

    private $username = null;

    private $password = null;

    private $shop_id = null;

    public function __construct($host, $firma, $username, $password, $shop_id)
    {
        $this->host = $host;
        $this->firma = $firma;
        $this->username = $username;
        $this->password = $password;

        $this->shop_id = $shop_id;

        Httpful\Bootstrap::init();

        // Create the template
        $template = Httpful\Request::init()
            // ->method(Http::POST)        // Alternative to Request::post
             ->withoutStrictSsl()        // Ease up on some of the SSL checks
            // ->expectsHtml()             // Expect HTML responses
            // ->sendsType(Mime::FORM);    // Send application/x-www-form-urlencoded
            ->authenticateWith($this->username, $this->password)
             ->expectsJson();

        // Set it as a template
        Httpful\Request::ini($template);

        $this->l = Log::singleton('file', dirname(__FILE__).'/log/FlexibeeRest.log');
    }

    /**
    * Metoda pro získání stavu skladu k datu
    * @author Jakub Stejskal
    * @copyright 2017
    *
    * @param $datum datum k získání stavu
    * @param $sklad identifikátor skladu
    **/
    public function getStavSkladuKDatu($datum = null, $sklad = null)
    {
        try {
            $datum = (is_null($datum) ? date("Y-m-d") : $datum);
            $sklad = (is_null($sklad) ? "code:SKLAD_HALA" : $sklad);

            $uri = $this->host.'/c/'.$this->firma.('/stav-skladu-k-datu.json?limit=100&datum=' . $datum . '&sklad=' . $sklad);
            //echo $uri;
            $aResponse = $this->_get($uri);

            $sObject = "stav-skladu-k-datu";
            return $aResponse->body->winstrom->{$sObject};
        } catch (Exception $e) {
            return false;
        }
    }

    private function _get($uri)
    {
        try {
            $response = \Httpful\Request::get($uri)->send();
            if ($response->hasErrors()) {
                throw new Exception($response->body->winstrom->message, 1);
                return false;
            } else {
                return $response;
            }
        } catch (Exception $e) {
            var_dump($e);
            throw new exception($e);
            return FALSE;
        }
    }

    private function _post($uri, $body)
    {
        #var_dump($uri);
        try {
            $response = \Httpful\Request::post($uri)->body($body)
                                                  ->sendsJson()
                                                  ->send();
            if ($response->hasErrors()) {
                #var_dump ($response);
                $this->l->log('_post error: '.print_r($response, true), PEAR_LOG_ERR);
                return false;
            } else {
                return $response->body->winstrom;
            }
        } catch (Exception $e) {
            var_dump($e);
            $this->l->log('_post exception: '.print_r($e, true), PEAR_LOG_ERR);
            return false;
        }
    }

    private function _put($uri, $body)
    {
        #var_dump($uri);
        $response = \Httpful\Request::put($uri)->body($body)
                                                ->sendsJson()
                                                ->send();

        return $response->body->winstrom;
    }

    private function _delete($uri)
    {
        #var_dump($uri);
        $response = \Httpful\Request::delete($uri)
                                                ->sendsJson()
                                                ->send();
        return $response->body->winstrom;
    }
}
