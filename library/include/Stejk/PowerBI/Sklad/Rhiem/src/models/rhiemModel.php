<?php
/**
 * RHIEM API Model
 */
namespace Stejk\PowerBI\RhiemSklad\Models;
class RhiemModel
{
    public $server;

    /**
     * RhiemModel constructor.
     */
    function __construct($type = "salesorders")
    {

      $wsdl = 'https://logisticapi.rhiem.com:4443/3_0_0/' . $type . '.svc?wsdl';
      $options = array(
          'trace' => 1,
          'exceptions' => true,
          'cache_wsdl' => WSDL_CACHE_NONE,
          //'soap_version' => SOAP_1_1,
      );

      $username = RHIEMSKLAD_USERNAME;
      $password = RHIEMSKLAD_PASSWORD;
      $wsseAuthHeader = new WsseAuthHeader($username, $password);

      $this->server = new \SoapClient($wsdl, $options);
      $this->server->__setSoapHeaders(array($wsseAuthHeader));
    }

    /**
     * @return Object Stocks
     */
    public function getStocks()
    {
      $request_API = array(
          "MessageId" => date("YmdHiu"),
          "CustomerId" => RHIEMSKLAD_CLIENT_ID,
      );
      $response = $this->server->__soapCall("Stocks", array($request_API));

      //echo(json_encode($response));
      //die();
      return $response->StocksResult->ItemStocks->ItemStock; // TODO: Předělat na kontrolu chyb
    }
}
