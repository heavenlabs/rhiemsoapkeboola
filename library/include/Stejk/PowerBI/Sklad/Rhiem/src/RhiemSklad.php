<?php

namespace Stejk\PowerBI;

class RhiemSklad implements \Stejk\PowerBI\SkladInterface
{
  public function getStav($type)
  {
    $rhiemModel = new \Stejk\PowerBI\RhiemSklad\Models\RhiemModel("items");


    $stavSklad = array();

    foreach ($rhiemModel->getStocks() as $polozka) {
        if($polozka->ArticleNumber == "03257"){
          $stavSklad["bag"] = $polozka->CurrentAvailable*7;
        } elseif($polozka->ArticleNumber == "8596172000099"){
          $stavSklad["drink"] = $polozka->CurrentAvailable * 12;
        }
    }

    return $stavSklad[$type];
  }
}
