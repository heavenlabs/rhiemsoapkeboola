<?php

namespace Stejk\PowerBI;

class Prediction
{
  public $sklad;

  public $stavSklad = array();

  public function __construct(\Stejk\PowerBI\SkladInterface $sklad)
  {

    $this->sklad = $sklad;

  }

  public function getSkladZasoby()
  {
    $this->stavSklad = array(
      "bag" => $this->sklad->getStav("bag"),
      "drink" => $this->sklad->getStav("drink")
    );
  }

  public function create()
  {
    $postFieldsObj = new \stdClass();
    $postFieldsObj->date = gmdate('Y-m-d\TH:i:s\Z');
    $postFieldsObj->skladDrinkActual = round($this->stavSklad["drink"]);
    $postFieldsObj->skladPowderActual = round($this->stavSklad["bag"]);

    return $postFieldsObj;
  }

}
