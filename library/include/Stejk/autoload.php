<?php

include_once 'PowerBI/Predict/History.php';
include_once 'PowerBI/Predict/Prediction.php';

// Sklady
include_once 'PowerBI/Sklad/SkladInterface.php';
// Flexibee
include_once 'PowerBI/Sklad/Flexibee/autoload.php';
// Rhiem
include_once 'PowerBI/Sklad/Rhiem/autoload.php';
// TotalWine
include_once 'PowerBI/Sklad/TotalWine/autoload.php';
