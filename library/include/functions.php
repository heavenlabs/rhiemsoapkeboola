<?php

function getAvarage($days = 30){
  $czConn = new mysqli("127.0.0.1", "wpzczxxwjx", "zDq3Qun4Xv", "wpzczxxwjx");
  $euConn = new mysqli("127.0.0.1", "zkdgbtuckn", "dc3QWAAQr5", "zkdgbtuckn");
  $stat = array(
    "bag" => 0,
    "drink" => 0
  );
  getStat($czConn, $stat, "cz", $days);
  getStat($euConn, $stat, "eu", $days);

  $return = array(
    "bag" => ($stat["bag"]/$days),
    "drink" => ($stat["drink"]/$days)
  );

  return $return;
}

function getStatsForDays($days = 30){
  $czConn = new mysqli("127.0.0.1", "wpzczxxwjx", "zDq3Qun4Xv", "wpzczxxwjx");
  $euConn = new mysqli("127.0.0.1", "zkdgbtuckn", "dc3QWAAQr5", "zkdgbtuckn");
  $stat = array(
    "bag" => 0,
    "drink" => 0
  );
  getStat($czConn, $stat, "cz", $days);
  getStat($euConn, $stat, "eu", $days);

  return $stat;
}

function getStat($connection, &$stat, $type = "cz", $days = 30){
    $dateTo = date("Y-m-d H:i:s");
    $dateFrom = date("Y-m-d H:i:s", strtotime($dateTo. ' - ' . $days . ' days'));

    $statuses = array();
    $statuses["cz"] = "IN(2, 3, 4, 5, 9, 12, 13, 14, 15, 17, 18, 19, 22, 24, 27, 29, 31, 32, 33, 34, 35, 36, 37, 38, 39, 41, 51, 53, 59, 60, 61, 62, 63, 64, 71, 72, 73, 74, 75, 76, 77)";
    $statuses["eu"] = "IN(2, 3, 4, 5, 9, 12, 13, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 29, 30)";

    $sql = 'SELECT
          COUNT(d.product_name) as Pocet,
          d.product_reference as Reference
        FROM
          ps_order_detail d
        LEFT JOIN
          ps_orders o
        ON
          d.id_order = o.id_order
        WHERE
          (o.current_state ' . $statuses[$type] . ')
          AND (o.date_add > "' . $dateFrom . '" AND o.date_add < "' . $dateTo . '")
        GROUP BY
          d.product_reference';

    $data = $connection->query($sql);


    foreach ($data as $product) {
      switch ($product["Reference"]) {
        // POWDER
        case '0001':
          $stat["bag"] = $stat["bag"] + (7 * $product["Pocet"]);
          break;
        case '0003':
          $stat["bag"] = $stat["bag"] + (14 * $product["Pocet"]);
          break;
        case '0006':
          $stat["bag"] = $stat["bag"] + (28 * $product["Pocet"]);
          break;
        case '0007':
          $stat["bag"] = $stat["bag"] + (28 * $product["Pocet"]);
          break;
        case '0010':
          $stat["bag"] = $stat["bag"] + (14 * $product["Pocet"]);
          break;
        case '0011':
          $stat["bag"] = $stat["bag"] + (7 * $product["Pocet"]);
          break;
        // DRINK
        case 'DR12':
          $stat["drink"] = $stat["drink"] + (12 * $product["Pocet"]);
          break;
        case 'DR24':
          $stat["drink"] = $stat["drink"] + (24 * $product["Pocet"]);
          break;
        case 'DR36':
          $stat["drink"] = $stat["drink"] + (36 * $product["Pocet"]);
          break;
        case 'DR48':
          $stat["drink"] = $stat["drink"] + (48 * $product["Pocet"]);
          break;
        case 'DR60':
          $stat["drink"] = $stat["drink"] + (60 * $product["Pocet"]);
          break;
        case 'DR72':
          $stat["drink"] = $stat["drink"] + (72 * $product["Pocet"]);
          break;
        case 'DR84':
          $stat["drink"] = $stat["drink"] + (84 * $product["Pocet"]);
          break;
        case 'DR96':
          $stat["drink"] = $stat["drink"] + (96 * $product["Pocet"]);
          break;
        case 'DR108':
          $stat["drink"] = $stat["drink"] + (108 * $product["Pocet"]);
          break;
        default:
          # code...
          break;
      }
    }
    //return $stat;
}
